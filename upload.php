<?php
	$dir = '/task/uploads/';
	$allowed = array('gif','jpg','jpeg','png');
	$currentTime = time();

	$text = $_POST['text'];

	//Check if editor is not empty
	if($text){
		//Load HTML 
		$doc = new DOMDocument();
		@$doc->loadHTML(mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8'));
		$images = $doc->getElementsByTagName('img');

		foreach ($images as $img) {
			$src = $img->getAttribute('src');
			$dataFileName = pathinfo($img->getAttribute('data-filename'));
			
			$fileName = $dataFileName['filename'];
			$fileExt = $dataFileName['extension'];

			//Get base64 code
			list(, $base64Code) = explode(";base64,", $src);

			//Check if type is allowed
			if(in_array($fileExt, $allowed)){
				$newFileName = $fileName . '-' . $currentTime . '.' . $fileExt;
				$fileRoute = $_SERVER['DOCUMENT_ROOT'] . $dir . $newFileName;

				//If file was successfully added get url and replace
				if(file_put_contents($fileRoute, base64_decode($base64Code))){
					$url = 'http://' . $_SERVER['HTTP_HOST'] .  $dir . $newFileName;

					$img->setAttribute('src', $url);
					$img->setAttribute('style', 'max-width:100%');
				}
			}
		}
		echo $doc->saveHTML();
	}
?>